# sunnyboy-datalogger #

NodeJS script which will log your SMA sunnyboy data to various destinations like pvoutput, influxdb and home assistant.

### Installation

> yarn install

Create config file `sunnyboy-datalogger.config.js` with the following content:

```text
module.exports = {
    "host": "IP ADDRESS OF SMA",
    "pass": "USER PASSWORD",
    "hass": {
        "host": "IP OF HOME ASSISTANT SERVER",
        "port": "8123",
        "token": "TOKE OF HOME ASSITANT SERVER"
    },
    "bugsnag": {
        "key": "******"
    }
}
``` 

### Credits

Forked from https://github.com/martijndierckx/sunnyboy-influxdb (sunnyboy to influx service)
