# Changelog

## [1.1.1]

* Extra logging to see why SMA interface is crashing

## [1.1.0]

* Added bugsnag cloud logging

## [1.0.0]

Initial release
