import axios from 'axios';
import * as https from 'https';
const Bugsnag = require('@bugsnag/js');

import {
    MyToolbox,
    SmaData,
    SmaGetMidnightYieldRequest,
    SmaGetMidnightYieldResponse,
    SmaGetValuesRequest,
    SmaGetValuesResponse,
    SmaKeys,
    SmaLoginRequest,
    SmaLoginResponse,
    SmaMainKey,
} from '../models';
import moment = require('moment');

module.exports = async (toolbox: MyToolbox) => {
    const {
        cache,
        print: { error },
    } = toolbox;

    const config = {
        host: undefined,
        pass: undefined,
        sid: undefined,
    };

    const customAxiosInstance = (host: string) => {
        return axios.create({
            baseURL: 'https://' + host,
            headers: {
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                Origin: 'https://' + host,
            },
            httpsAgent: new https.Agent({
                rejectUnauthorized: false,
            }),
        });
    };

    function getValue(data: SmaGetValuesResponse, key: SmaKeys, index = 0): number {
        if (
            data.result.hasOwnProperty(SmaMainKey) &&
            data.result[SmaMainKey].hasOwnProperty(key) &&
            data.result[SmaMainKey][key].hasOwnProperty(1) &&
            data.result[SmaMainKey][key][1].length >= index + 1
        ) {
            return data.result[SmaMainKey][key][1][index].val;
        }
        return 0;
    }

    function getWatt(data: SmaGetValuesResponse, key: SmaKeys, index = 0): number {
        return getValue(data, key, index);
    }

    function getKwh(data: SmaGetValuesResponse, key: SmaKeys, index = 0): number {
        return getValue(data, key, index) / 1000;
    }

    function getVolt(data: SmaGetValuesResponse, key: SmaKeys, index = 0): number {
        return getValue(data, key, index) / 100;
    }

    function getAmp(data: SmaGetValuesResponse, key: SmaKeys, index = 0): number {
        return getValue(data, key, index) / 1000;
    }

    function login(host: string, pass: string, noCache = false): Promise<string> {
        config.host = host;
        config.pass = pass;
        return new Promise(async (resolve, reject) => {
            if (!noCache) {
                const token = await cache.get('token');
                if (token) {
                    config.sid = token;
                    return resolve(token);
                }
            }

            const { post } = toolbox.http.create({ baseURL: undefined, axiosInstance: customAxiosInstance(host) });
            const request: SmaLoginRequest = {
                right: 'usr',
                pass,
            };
            const { ok, status, data } = await post('/dyn/login.json', request);

            if (ok && status === 200 && (data as SmaLoginResponse).result && (data as SmaLoginResponse).result.sid) {
                await cache.set('token', (data as SmaLoginResponse).result.sid);
                config.sid = (data as SmaLoginResponse).result.sid;
                console.log(new Date().toISOString() + '::login');
                return resolve(config.sid);
            }

            console.log(new Date().toISOString() + '::login failed');
            Bugsnag.notify(
                {
                    name: 'SMA|login',
                    message: JSON.stringify({ ok, status, data }, null, 2),
                },
                undefined,
                () => {
                    error('Unable to login');
                    reject();
                },
            );
        });
    }

    function getMidnightYield(): Promise<number> {
        return new Promise(async (resolve, reject) => {
            const { post } = toolbox.http.create({ baseURL: undefined, axiosInstance: customAxiosInstance(config.host) });
            const start = moment().utc().startOf('day').subtract(1, 'days');
            const end = start.clone().add(1, 'days');
            const request: SmaGetMidnightYieldRequest = {
                destDev: [],
                key: 28704,
                tStart: start.unix(),
                tEnd: end.unix(),
            };

            const { ok, status, data } = await post('/dyn/getLogger.json?sid=' + config.sid, request);

            if (ok && status === 200) {
                console.log(new Date().toISOString() + '::getMidnightYield');
                if ((data as SmaGetMidnightYieldResponse).err) {
                    login(config.host, config.pass, true)
                        .then(() => {
                            getMidnightYield()
                                .then((midnightYield) => resolve(midnightYield))
                                .catch((err) => reject(err));
                        })
                        .catch((err) => reject(err));
                    return;
                }

                const nyield =
                    (data as SmaGetMidnightYieldResponse).result[SmaMainKey][0] === undefined
                        ? 0
                        : (data as SmaGetMidnightYieldResponse).result[SmaMainKey][0].v;
                return resolve(nyield);
            }
            console.log(new Date().toISOString() + '::getMidnightYield FAILED', status, data);
            Bugsnag.notify(
                {
                    name: 'SMA|getMidnightYield',
                    message: JSON.stringify({ ok, status, data }, null, 2),
                },
                undefined,
                () => {
                    reject();
                },
            );
        });
    }

    toolbox.sma = {
        getValues: (host: string, pass: string): Promise<SmaData> => {
            return new Promise(async (resolve, reject) => {
                login(host, pass)
                    .then(async () => {
                        getMidnightYield()
                            .then(async (midnightYield) => {
                                const { post } = toolbox.http.create({ baseURL: undefined, axiosInstance: customAxiosInstance(host) });
                                const request: SmaGetValuesRequest = {
                                    destDev: [],
                                };

                                const { ok, status, data } = await post('/dyn/getAllOnlValues.json?sid=' + config.sid, request);

                                if (ok && status === 200) {
                                    console.log(new Date().toISOString() + '::getValues');
                                    if ((data as SmaGetValuesResponse).err) {
                                        login(host, pass, true)
                                            .then(() => {
                                                toolbox.sma
                                                    .getValues(host, pass)
                                                    .then((values) => resolve(values))
                                                    .catch((err) => reject(err));
                                            })
                                            .catch((err) => reject(err));
                                        return;
                                    }

                                    const dayYield = (getValue(data, SmaKeys.TotalYield) - midnightYield) / 1000;
                                    const smaData: SmaData = {
                                        dc: {
                                            string1: {
                                                watt: getWatt(data, SmaKeys.DC_Watt),
                                                volt: getVolt(data, SmaKeys.DC_Volt),
                                                amp: getAmp(data, SmaKeys.DC_Amp),
                                            },
                                            string2: {
                                                watt: getWatt(data, SmaKeys.DC_Watt, 1),
                                                volt: getVolt(data, SmaKeys.DC_Volt, 1),
                                                amp: getAmp(data, SmaKeys.DC_Amp, 1),
                                            },
                                        },
                                        ac: {
                                            watt: getWatt(data, SmaKeys.AC_Watt),
                                            frequency: getValue(data, SmaKeys.AC_Frequency),
                                            L1: {
                                                volt: getVolt(data, SmaKeys.L1_Volt),
                                                amp: getAmp(data, SmaKeys.L1_Amp),
                                            },
                                            L2: {
                                                volt: getVolt(data, SmaKeys.L2_Volt),
                                                amp: getAmp(data, SmaKeys.L2_Amp),
                                            },
                                            L3: {
                                                volt: getVolt(data, SmaKeys.L2_Volt),
                                                amp: getAmp(data, SmaKeys.L2_Amp),
                                            },
                                            L1L2: {
                                                volt: getVolt(data, SmaKeys.L1_L2_Volt),
                                            },
                                            L2L3: {
                                                volt: getVolt(data, SmaKeys.L2_L3_Volt),
                                            },
                                            L3L1: {
                                                volt: getVolt(data, SmaKeys.L3_L1_Volt),
                                            },
                                        },
                                        dayYield: {
                                            kwh: dayYield,
                                        },
                                        totalYield: {
                                            kwh: getKwh(data, SmaKeys.TotalYield),
                                        },
                                    };
                                    return resolve(smaData);
                                }
                                console.log(new Date().toISOString() + '::getValues FAILED', status, data);
                                Bugsnag.notify(
                                    {
                                        name: 'SMA|getValues',
                                        message: JSON.stringify({ ok, status, data }, null, 2),
                                    },
                                    undefined,
                                    () => {
                                        reject();
                                    },
                                );
                            })
                            .catch((err) => reject(err));
                    })
                    .catch((err) => reject(err));
            });
        },
    };
};
