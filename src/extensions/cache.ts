import { SnapDB } from 'snap-db';
import { MyToolbox } from '../models';

module.exports = async (toolbox: MyToolbox) => {
    const { filesystem } = toolbox;
    const db = new SnapDB(filesystem.path(process.cwd(), '.cache'));
    toolbox.cache = {
        get: (key: string): Promise<string> => {
            return db.get(key);
        },
        set: (key: string, value: string): Promise<any> => {
            return db.put(key, value);
        },
        clear: (): Promise<void> => {
            return db.empty()
        }
    };
};
