import { MyToolbox } from '../models';

module.exports = {
    name: 'check-config',
    description: 'Check configuration file',
    alias: [],
    run: async (toolbox: MyToolbox) => {
        const {
            config,
            print: { info, error },
        } = toolbox;

        let allOk = true;

        if (!config.hasOwnProperty('host')) {
            error('Configuration entry for host is missing');
            allOk = false;
        }

        if (!config.hasOwnProperty('pass')) {
            error('Configuration entry for pass is missing');
            allOk = false;
        }

        if (toolbox.hasOwnProperty('hass') && !config.hasOwnProperty('hass')) {
            error('Configuration for home-assistant is missing');
            allOk = false;
        }

        if (allOk) {
            info('Configuration OK');
        } else {
            info('Run "sunnyboy-datalogger init" to create the configuration');
        }
        process.exit(0);
    },
};
