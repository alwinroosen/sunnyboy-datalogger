import { Config, MyToolbox } from '../models';

module.exports = {
    name: 'init',
    description: 'Create configuration file',
    alias: [],
    run: async (toolbox: MyToolbox) => {
        const {
            prompt: { ask },
            print: { info },
            filesystem,
        } = toolbox;

        const askHost = { type: 'input', name: 'host', message: 'Hostname or IP of your SMA device?' };
        const askPass = { type: 'input', name: 'pass', message: 'Password for the web-interface?' };
        const questions = [askHost, askPass];
        const { host, pass } = await ask(questions);
        const config: Config = {
            host,
            pass,
        };

        filesystem.write(filesystem.path(process.cwd(), 'sunnyboy-datalogger.config.js'), 'module.exports = ' + JSON.stringify(config, null, 4));

        info('Configuration file "sunnyboy-datalogger.config.js" saved.');
    },
};
