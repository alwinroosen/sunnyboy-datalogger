import { MyToolbox } from '../models';

module.exports = {
    name: 'run',
    description: 'Fetch data from SMA and export through activated plugins',
    alias: [],
    run: async (toolbox: MyToolbox) => {
        const {
            cache: { clear },
            sma: { getValues },
        } = toolbox;

        await clear();

        const run = () => {
            getValues(toolbox.config.host, toolbox.config.pass)
                .then(async (data) => {
                    if (toolbox.hasOwnProperty('hass')) {
                        await toolbox.hass.export(data);
                    }
                })
                .catch((err) => {
                    console.error('Unable to get values', err);
                });
        };

        setInterval(run, 5000);
    },
};
