import { GluegunToolbox } from 'gluegun';

export interface Config {
    host: string;
    pass: string;
}

export interface MyToolbox extends GluegunToolbox {
    config: Config;
    cache: {
        get: (key: string) => Promise<string>;
        set: (key: string, value: string) => Promise<any>;
        clear: () => Promise<void>;
    };
    sma: {
        getValues: (host: string, pass: string) => Promise<SmaData>;
    };
    hass?: {
        export: (data: SmaData) => Promise<void>;
    };
    influx?: {
        export: (data: SmaData) => Promise<void>;
    };
    pvoutput?: {
        export: (data: SmaData) => Promise<void>;
    };
}

export interface SmaData {
    dc: {
        string1: {
            watt: number;
            volt: number;
            amp: number;
        };
        string2: {
            watt: number;
            volt: number;
            amp: number;
        };
    };
    ac: {
        watt: number;
        frequency: number;
        L1: {
            volt: number;
            amp: number;
        };
        L2: {
            volt: number;
            amp: number;
        };
        L3: {
            volt: number;
            amp: number;
        };
        L1L2: {
            volt: number;
        };
        L2L3: {
            volt: number;
        };
        L3L1: {
            volt: number;
        };
    };
    dayYield: {
        kwh: number;
    };
    totalYield: {
        kwh: number;
    };
}

export interface SmaLoginRequest {
    right: 'istl' | 'usr';
    pass: string;
}

export interface SmaLoginResponse {
    result: {
        sid: string | null;
    };
}

export interface SmaGetMidnightYieldRequest {
    destDev: any[];
    key: number;
    tStart: number;
    tEnd: number;
}

export interface SmaGetMidnightYieldResponse {
    err?: number;
    result?: any;
}

export interface SmaGetValuesRequest {
    destDev: any[];
}

export interface SmaGetValuesResponseDataEntry {
    1: {
        val: number;
    }[];
}

export const SmaMainKey = '0198-B339E13E';

export enum SmaKeys {
    TotalYield = '6400_00260100',
    DC_Watt = '6380_40251E00',
    DC_Volt = '6380_40451F00',
    DC_Amp = '6380_40452100',
    AC_Watt = '6100_40263F00',
    AC_Frequency = '6100_00465700',
    L1_Volt = '6100_00464800',
    L1_Amp = '6100_40465300',
    L2_Volt = '6100_00464900',
    L2_Amp = '6100_40465400',
    L3_Volt = '6100_00464A00',
    L3_Amp = '6100_40465500',
    L1_L2_Volt = '6100_00464B00',
    L2_L3_Volt = '6100_00464C00',
    L3_L1_Volt = '6100_00464D00',
}

export interface SmaGetValuesResponse {
    err?: number;
    result?: {
        [mainKey: string]: {
            [key: string]: SmaGetValuesResponseDataEntry;
        };
    };
}
