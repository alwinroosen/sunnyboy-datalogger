const { build } = require('gluegun');
const Bugsnag = require('@bugsnag/js');
const path = require('path');
const devMode = require('fs').existsSync(`${__dirname}/../src`);
const config = require(path.resolve(__dirname, (devMode ? '../' : '') + 'sunnyboy-datalogger.config.js'));

if (config && config.bugsnag && config.bugsnag.key) {
    Bugsnag.start({ apiKey: config.bugsnag.key });
    process.on('uncaughtException', (err) => {
        console.error('Caught exception', err);
        Bugsnag.notify(err);
    });
}

async function run(argv) {
    const cli = build('sunnyboy-datalogger')
        .src(`${__dirname}`)
        .plugin('plugins/home-assistant')
        .plugin('plugins/influxdb')
        .plugin('plugins/pvoutput')
        .plugins('node_modules', { matching: 'sunnyboy-datalogger-*' })
        .help()
        .version()
        .defaultCommand()
        .create();

    return cli.run(argv);
}

module.exports = { run };
