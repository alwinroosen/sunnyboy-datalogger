const path = require('path');
const Deployer = require('ssh-deploy-release');
const config = require(path.resolve(__dirname, '../', 'sunnyboy-datalogger.config.js'));


const options = {
    localPath: 'dist',
    currentReleaseLink: 'latest',
    ...config.ssh,
    onBeforeDeploy: 'cd ' + config.ssh.deployPath + '/latest' + ' && mv node_modules ../',
    onAfterDeploy: 'cd ' + config.ssh.deployPath + '/latest' + ' && mv ../node_modules . && npm install'
};

const deployer = new Deployer(options);

deployer.deployRelease(() => {
    console.log('Ok !')
});
