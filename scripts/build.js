const path = require('path');
const fs = require('fs');

let packageJson = require(path.resolve(__dirname, '../', 'package.json'));

packageJson =  {
    name: packageJson.name,
    version: packageJson.version,
    description: packageJson.description,
    repository: packageJson.repository,
    license: packageJson.license,
    dependencies: packageJson.dependencies
};

fs.copyFileSync(path.resolve(__dirname, '../', 'sunnyboy-datalogger.config.js'), path.resolve(__dirname, '../', 'dist', 'sunnyboy-datalogger.config.js'));
fs.writeFileSync(path.resolve(__dirname, '../', 'dist', 'cli.js'), 'require(\'./sunnyboy-datalogger\').run(process.argv);');
fs.writeFileSync(path.resolve(__dirname, '../', 'dist', 'package.json'), JSON.stringify(packageJson, null, 2));
