import { SmaData } from '../../../src/models';

module.exports = (toolbox) => {
    toolbox.hass = {
        export: async (smaData: SmaData) => {
            const { post } = toolbox.http.create({
                baseURL: 'http://' + toolbox.config.hass.host + ':' + toolbox.config.hass.port + '/api',
                headers: {
                    Authorization: 'Bearer ' + toolbox.config.hass.token,
                    'Content-Type': 'application/json',
                },
            });

            const uploadDataEntry = async (entity_id: string, value: number, name: string, uom: string) => {
                entity_id = 'sensor.sma_' + entity_id;
                const request = {
                    state: value,
                    attributes: {
                        unit_of_measurement: uom,
                        friendly_name: name,
                    },
                };
                await post('/states/' + entity_id, request);
            };

            await uploadDataEntry('total_yield', smaData.totalYield.kwh, 'Total Yield', 'kWh');
            await uploadDataEntry('day_yield', smaData.dayYield.kwh, 'Day Yield', 'kWh');
            await uploadDataEntry('dc_string1_watt', smaData.dc.string1.watt, 'DC String 1 Watt', 'W');
            await uploadDataEntry('dc_string1_volt', smaData.dc.string1.volt, 'DC String 1 Volt', 'V');
            await uploadDataEntry('dc_string1_amp', smaData.dc.string1.amp, 'DC String 1 Amp', 'A');
            await uploadDataEntry('dc_string2_watt', smaData.dc.string2.watt, 'DC String 2 Watt', 'W');
            await uploadDataEntry('dc_string2_volt', smaData.dc.string2.volt, 'DC String 2 Volt', 'V');
            await uploadDataEntry('dc_string2_amp', smaData.dc.string2.amp, 'DC String 2 Amp', 'A');
            await uploadDataEntry('ac_watt', smaData.ac.watt, 'Current Generation', 'W');
            await uploadDataEntry('ac_frequency', smaData.ac.frequency, 'Current Frequency', 'Hz');
            await uploadDataEntry('ac_l1_volt', smaData.ac.L1.volt, 'AC L1 Volt', 'V');
            await uploadDataEntry('ac_l1_amp', smaData.ac.L1.amp, 'AC L1 Amp', 'A');
            await uploadDataEntry('ac_l2_volt', smaData.ac.L2.volt, 'AC L2 Volt', 'V');
            await uploadDataEntry('ac_l2_amp', smaData.ac.L2.amp, 'AC L2 Amp', 'A');
            await uploadDataEntry('ac_l3_volt', smaData.ac.L3.volt, 'AC L3 Volt', 'V');
            await uploadDataEntry('ac_l3_amp', smaData.ac.L3.amp, 'AC L3 Amp', 'A');
            await uploadDataEntry('ac_l1l2_volt', smaData.ac.L1L2.volt, 'AC L1/L2 Volt', 'V');
            await uploadDataEntry('ac_l2l3_volt', smaData.ac.L2L3.volt, 'AC L2/L3 Volt', 'V');
            await uploadDataEntry('ac_l3l1_volt', smaData.ac.L3L1.volt, 'AC L3/L1 Volt', 'V');

            console.log('Data entries uploaded to HASS');
        },
    };
};
